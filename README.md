# photos

project to order photo files

## Getting started

the class name is Photo which has 6 methods to validate and process the string to generate the string formatted

methods
 - solutions: return the string asked for documentation and call other methods to settle the array, validate the input string and generate the respective arrays
 - validations: this method makes validations required by documentation
 - settle_array: generate the array and verify if the string has the right format
 - repeated: create a hash with cities as keys and values as the number of times the city name is repeated in the array
 - get_dates: generate a hash with cities as keys and values as arrays of dates given in the array
 - names: generate an array with the output requested by documentation


