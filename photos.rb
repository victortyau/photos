require 'date'

class Photo
  def solutions(string)
    return "wrong string" unless string.include?("\n")
    array = validations(settle_array(string))
    if array.count.between?(1, 100)
      occurrences = repeated(array)
      dates = get_dates(array)
      names(array, dates, occurrences).join("\n")
    else
      return "problems with the array"
    end
  end

  def validations(array)
    array.map do |values|
      date = DateTime.strptime(values[2], '%Y-%m-%d %H:%M:%S')
      if date.year.between?(2000, 2020) &&
         values[1].length.between?(1, 20) &&
         values[0].gsub(File.extname(values[0]), "").length.between?(1, 20) &&
         values[1] == values[1].capitalize &&
         ["jpg","png","jpeg"].include?(File.extname(values[0]).gsub('.', ''))
        values
      end
    end.compact
  end

  def settle_array(string)
    regex = Regexp.new('[A-Za-z0-9]+(.jpg|.png|.jpeg), [A-Za-z]+, [0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?')
    string.strip.split("\n").map {|lines| lines if regex.match?(lines) }.compact.map {|array| array.split(", ") }.uniq {|values| [values[1], values[2]] }
  end

  def repeated(array)
    array.map {|values| values[1] }.group_by(&:itself).transform_values(&:count)
  end

  def get_dates(array)
    array.map {|values| [values[1],values[2]] }.group_by{|val| val.shift }.transform_values{|values| values.flatten.sort! }
  end

  def names(array, dates, occurrences)
     array.map do |values|
      number = ( dates[values[1]].index(values[2]) + 1).to_s.rjust(occurrences[values[1]].to_s.length, "0")
      values[1]+(number)+File.extname(values[0])
    end
  end
end

photo = Photo.new
puts photo.solutions("photo.jpg, Krakow, 2013-09-05 14:08:15
Mike.png, London, 2015-06-20 15:13:22
myFriends.png, Krakow, 2013-09-05 14:07:13
Eiffel.jpg, Florianopolis, 2015-07-23 08:03:02
pisatower.jpg, Florianopolis, 2015-07-22 23:59:59
BOB.jpg, London, 2015-08-05 00:02:03
notredame.png, Florianopolis, 2015-09-01 12:00:00
me.jpg, Krakow, 2013-09-06 15:40:22
a.png, Krakow, 2016-02-13 13:33:50
b.jpg, Krakow, 2016-01-02 15:12:22
c.jpg, Krakow, 2016-01-02 14:34:30
d.jpg, Krakow, 2016-01-02 15:15:01
e.png, Krakow, 2016-01-02 09:49:09
f.png, Krakow, 2016-01-02 10:55:32
g.jpg, Krakow, 2016-02-29 22:13:11")  